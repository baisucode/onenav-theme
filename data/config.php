<?php
//载入数据库配置
require 'class/Medoo.php';
use Medoo\Medoo;
$db = new medoo([
    'database_type' => 'sqlite',
    'database_file' => 'data/onenav.db3'
]);

//用户名
define('USER','admin');
//密码
define('PASSWORD','admin');
//邮箱，用于后台Gravatar头像显示
define('EMAIL','1099116749@qq.com');
//token参数，API需要使用
define('TOKEN','admin');
//主题风格
define('TEMPLATE','baisu');

//站点信息
$site_setting = [];
//站点标题
$site_setting['title']          =   '百素导航';
//文字Logo
$site_setting['logo']          =   '百素导航';
//站点关键词
$site_setting['keywords']       =   '';
//站点描述
$site_setting['description']    =   '';

//这两项不要修改
$site_setting['user']           =   USER;
$site_setting['password']       =   PASSWORD;