<!DOCTYPE html><html>	<head>		<meta charset="utf-8" />
		<meta name="renderer" content="webkit|ie-comp|ie-stand">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
		<meta http-equiv="Cache-Control" content="no-transform">
		<meta name="applicable-device" content="pc,mobile">
		<meta name="MobileOptimized" content="width">
		<meta name="HandheldFriendly" content="true">
		<meta name="author" content="BaiSu" />
		<title>
			<?php echo $site_setting['title']; ?>		</title>		<meta name="keywords" content="<?php echo $site_setting['keywords']; ?>" />		<meta name="description" content="<?php echo $site_setting['description']; ?>" />		<link rel="stylesheet" type="text/css" href="templates/<?php echo TEMPLATE; ?>/css/style.css" />		<link rel="stylesheet" type="text/css" href="templates/<?php echo TEMPLATE; ?>/css/font-awesome.min.css"/>
		<?php echo $onenav['extend']; ?>
	</head>

	<body>
		<header>
			<div class="logo">
				<a href="/"><?php echo $site_setting['logo']; ?></a>
			</div>
			<div class="typelist">
				<?php			foreach ($categorys as $category) {			?>
					<p>
						<a href="#category-<?php echo $category['id']; ?>">
							<?php echo $category['name']; ?>
						</a>
					</p>
					<?php } ?>	<?php		if( is_login() ) {	  ?>				<p class="login">	<a href="/index.php?c=admin" title="后台管理" target="_blank"><i class="fa fa-user-circle-o"></i>后台管理</a></p>					<?php }else{ ?>					<p class="login"><a href="/index.php?c=login" title="登录" target="_blank"><i class="fa fa-user-circle-o"></i>管理登陆</a></p>					<?php } ?>
			</div>
		</header><div class="header">	<div class="logo">				<a href="/"><?php echo $site_setting['logo']; ?></a>			</div>			<div class="nav-bar">				<i class="fa fa-bars"></i>			</div></div>
		<nav class="main-top">
			<div class="search">
				<input type="text" class="search-input" placeholder="输入关键词搜索" />
				<button id="baidu">百度搜索</button>
			</div>
			<div class="main-top-r">				<span class="theme"><i class="fa fa-magic"></i></span>				<?php
		if( is_login() ) {
	  ?>
					<span class="bs-addUrl"><i class="fa fa-plus"></i></span>

					<a href="/index.php?c=admin" title="后台管理" target="_blank"><i class="fa fa-user-circle-o"></i></a>
					<?php }else{ ?>
					<a href="/index.php?c=login" title="登录" target="_blank"><i class="fa fa-user-circle-o"></i></a>
					<?php } ?>
			</div>
		</nav>
		<div class="main">
			<div class="site-main">
				<!-- 遍历分类目录 -->
				<?php foreach ( $categorys as $category ) {
                $fid = $category['id'];
                $links = get_links($fid);
                //如果分类是私有的
                if( $category['property'] == 1 ) {
                    $property = '<span><i class="fa fa-low-vision"></i></span>';
                }
                else {
                    $property = '';
                }
            ?>
				<div class="site-type" id="category-<?php echo $category['id']; ?>">
					<?php echo $category['name']; ?>
					<?php echo $property; ?>
				</div>
				<div class="site-list">
					<!-- 遍历链接 -->
					<?php
				foreach ($links as $link) {
					//默认描述
					$link['description'] = empty($link['description']) ? '作者很懒，没有填写描述。' : $link['description'];
					
				//var_dump($link);
			?>
						<div class="list url-list" id = "id_<?php echo $link['id']; ?>" link-title = "<?php echo $link['title']; ?>" link-url = "<?php echo $link['url']; ?>">
							<a href="/index.php?c=click&id=<?php echo $link['id']; ?>" target="_blank" title="<?php echo $link['description']; ?>">
								<p class="name">
									<img src="https://favicon.rss.ink/v1/<?php echo base64($link['url']); ?>">
									<?php echo $link['title']; ?>
								</p>
								<!--站点描述-->
								<!--<p class="desc">
									<?php echo $link['description']; ?>
								</p>-->
								<?php if($link['property'] == 1 ) { ?>
								<span class="private"><i class="fa fa-low-vision"></i></span>
								<?php } ?>

							</a>
						</div>
						<?php } ?>
						<!-- 遍历链接END -->

						<div class="list kongs"></div>
						<div class="list kongs"></div>
						<div class="list kongs"></div>
						<div class="list kongs"></div>
				</div>
				<?php } ?>
			</div>
		</div>

<div class="footer"></div>
<footer>		© 2021 BaiSu，Powered by <a target="_blank" href="https://github.com/helloxz/onenav" title="简约导航/书签管理器" rel="nofollow">OneNav</a>		<br />		The theme author is <a href="https://gitee.com/baisucode/onenav-theme" target="_blank">BaiSu</a>
		
	</footer><!--右下角--><div class="tool-bars">			<?php		if( is_login() ) {	  ?>	
	<p class="bs-addUrl"><i class="fa fa-plus"></i></p>	<?php }else{ ?>					<?php } ?>
	<p class="scroll_top"><i class="fa fa-chevron-up"></i></p>
</div>
<!--JS-->
		<script src="templates/<?php echo TEMPLATE; ?>/js/jquery-3.5.1.min.js" type="text/javascript" charset="utf-8"></script>		<script src="templates/<?php echo TEMPLATE; ?>/layer/layer.js" type="text/javascript" charset="utf-8"></script>		<script src="templates/<?php echo TEMPLATE; ?>/js/holmes.js" type="text/javascript" charset="utf-8"></script>		<script src="templates/<?php echo TEMPLATE; ?>/js/jquery.cookie.js" type="text/javascript" charset="utf-8"></script>		<script src="templates/<?php echo TEMPLATE; ?>/js/index.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">	var bodyh = $(document.body).height();	var htmlh = $(window).height();	if (bodyh>htmlh) {		$('footer').addClass('show')
	} else{		$('footer').removeClass('show')
	}</script>


	</body>

</html>