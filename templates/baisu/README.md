#不更新了  请使用  [baisuTwo](http://https://gitee.com/baisucode/baisu-two)
# OneNav主题
onenav导航的主题
![输入图片说明](https://images.gitee.com/uploads/images/2021/0409/202101_ea558ceb_1718725.png "1617970161549.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0409/202120_b0ca6d77_1718725.png "1617970194446.png")

默认不显示站点描述，

如需显示站点描述

/templates/index.php搜索

```
<?php echo $link['description']; ?>
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/0409/202133_35c7791a_1718725.png "1617970493224.png")

取消注释即可，如下图

![输入图片说明](https://images.gitee.com/uploads/images/2021/0409/202150_bffc1c7e_1718725.png "1617970511667.png")
## 分类图标
分类图标为font-awesome
图标地址[font-awesome](http://www.fontawesome.com.cn/icons-ui/)

## 一键添加
![输入图片说明](https://images.gitee.com/uploads/images/2021/0410/112213_3a134ad6_1718725.gif "a.gif")

在浏览器标签栏添加新标签
标签名称栏随意填写
标签地址栏，填写一下地址代码

```
javascript: var url = location.href;
var title = document.title;
void(open('http://www.你的域名.com/index.php?c=admin&page=add_quick_tpl&url=' + encodeURIComponent(url) + '&title=' + encodeURIComponent(title), "_blank", "toolbar=yes, location=yes, directories=no, status=no, menubar=yes, scrollbars=yes, resizable=no, copyhistory=yes, left=200,top=200,width=400, height=460"));
```
注意域名要替换成你的域名，然后保存即可。


## 相关链接

* [OneNav官网](https://nav.rss.ink/)
* [onenav作者](https://www.xiaoz.me/)